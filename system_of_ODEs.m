%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%           * Computational Assignment 2.
%
%           * Author : Juseok Lee
%           * Sep 2017
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%           * DEFINITIONS
%           * Purpose of my function is to  
%             solve a system of ODE within limited condtions.
%           * C(1)=CA, C(2)=CB which describe the mass concentrations of species A and B subject
%           * the units are hours for t and mg L-1 for CA and CB
%           * C is a 1x2 matrix (row vector) of concentrations
%           * dCA denotes dCA/dt, dCB denotes dCB/dt
%
%%
% ${ Developing \hspace{0.1in} MATLAB \hspace{0.1in} function \hspace{0.1in} to \hspace{0.1in} ODE \hspace{0.1in} using  \hspace{0.1in}VARARGIN. }$
%%
% $\frac{dC_A}{dt}=-k_1 C_A-k_2 C_A$
%%
% $\frac{dC_B}{dt}=k_1 C_A-k_3 -k_4 C_B$
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function output = system_of_ODEs(varargin)
% make a matrix
C=zeros(1,2);
    k1=0.15;
    k2=0.6;
    k3=0.1;
    k4=0.2;
% Case1
    if nargin==0
    C(1)=6.25;
    C(2)=0;
    dCA=-k1*C(1)-k2*C(1);
    dCB=k1*C(1)-k3-k4*C(2);
%Case2
      else if nargin<=2
            varargin(1)=t;
            varargin(2)=C;
    
            dCA=-k1*C(1)-k2*C(1);
            dCB=k1*C(1)-k3-k4*C(2);
%Case3
      else if nargin==6
              varargin(1)=t;
              varargin(2)=C;
              varargin(3)=k1;
              varargin(4)=k2;
              varargin(5)=k3;
              varargin(6)=k4;       
              
              dCA=-k1*C(1)-k2*C(1);
              dCB=k1*C(1)-k3-k4*C(2);
      end
      end
  output=[dCA;dCB];    

  end
  
  